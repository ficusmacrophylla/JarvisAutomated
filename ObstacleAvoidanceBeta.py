from megapi import *

gertrude=1
giappone=[1,2,3]

def move(command):
	print(command)
	if command == 'go':
		bot.motorRun(M1,85)
		bot.motorRun(M2,50)
	if command == 'stop':
		bot.motorRun(M1,0)
		bot.motorRun(M2,0)
	if command == 'back':
		bot.motorRun(M1,-85)
		bot.motorRun(M2,-50)
		sleep(2)
		bot.motorRun(M1,0)
		bot.motorRun(M2,0)

def getDist(v):
	global gertrude
	gertrude=float(str(v))

if __name__ == '__main__':
	bot = MegaPi()
	bot.start()
	bot.motorRun(M1,0)
	bot.motorRun(M2,0)
	sleep(1)
	while 1:
		sleep(1)
		print("breakpoint 1")
		bot.ultrasonicSensorRead(4,getDist)
		print(gertrude)
		sleep(3)
		while gertrude > 10:
			move("go")
			bot.ultrasonicSensorRead(4,getDist)
		move("stop")
		move("back")
		print(gertrude)
		print(giappone)
		break

#nota: i motori possiedono uno sfasamento, pertanto devono essere mantenuti a 35% di potenza in piu per il motore M1
#diversamente il rover tendera a destra durante il funzionamento. Si consiglia di sostituire il motore difettoso