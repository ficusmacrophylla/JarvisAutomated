from megapi import*
gPos=1 #global position (could be 0=N,1=S,2=E,3=W)
distance=0 #measure of the ultrasonic sensor in cm
tolerance=10 #free space tolerance in cm
direction=0
dirList=[0,0,0,0] #direction: 0 front, 1 right, 2 back, 3 left

def startup(bot):
    print("startup successuflly, starting...")

def updateDist(v): #obtain proximity measure and updates distance variable
    # print("distance:" + str(v) + " cm")
    global distance
    distance = str(v)

def evalDist(dist):
    if(dist>tolerance):
        return(1) #goes on
    else:
        return(0)

def lookNear(bot):
    frontD=backD=rightD=leftD=-1
    global dirList
    move("R")
    bot.ultrasonicSensorRead(4, updateDist)
    dirList[1]=distance
    move("R")
    bot.ultrasonicSensorRead(4, updateDist)
    dirList[2]=distance
    move("R")
    bot.ultrasonicSensorRead(4, updateDist)
    dirList[3]=distance
    move("R")
    bot.ultrasonicSensorRead(4, updateDist)
    dirList[0]=distance
    max=dirList[0]
    for d in dirList:
            if d > max:
                max=d
    return dirList.index(max)


def move(dir):
    if dir=="R" or dir==1: #turn right
        bot.motorRun(M1,10)
        sleep(5)
        bot.motorRun(M1, 0)
    elif dir=="L" or dir==2: #turn left
        bot.motorRun(M2, 10)
        sleep(5)
        bot.motorRun(M2, 0)
    elif dir=="F" or dir==0: #go forward
        bot.motorMove(50,50)
    elif dir == "B" or dir==3: #go backward
        bot.motorMove(-50, -50)
    else:
        print("navigation input error")


# if __name__ == '__main__':
# 	bot = MegaPi()
# 	bot.start()
# 	bot.motorRun(M1,0);
# 	bot.motorRun(M2,0);
# 	sleep(1);
#


if __name__ == '__main__':
    bot = MegaPi()
    bot.start()
    while 1:
        sleep(0.1)
        #startup(bot)
        bot.ultrasonicSensorRead(4, updateDist)
        if (evalDist(distance)): #valuta la distanza frontale
            move(dir) #se sufficiente avanza di quella distanza - offset
        else:
            direction = lookNear(bot)
            move(direction)
